import os

# Initialize a dictionary to store customer information
customer_info = {}

# Define a function to create a new customer account
def create_customer():
    first_name = input("Enter First Name: ")
    last_name = input("Enter Last Name: ")
    email = input("Enter Email: ")
    account_number = len(customer_info) + 1
    customer_info[account_number] = {"First Name": first_name, "Last Name": last_name, "Email": email, "Savings Account": 0, "Current Account": 0}
    print("Customer account created. Account Number:", account_number)

# Define a function to delete a customer account
def delete_customer():
    account_number = int(input("Enter Account Number to Delete: "))
    if customer_info[account_number]["Savings Account"] == 0 and customer_info[account_number]["Current Account"] == 0:
        del customer_info[account_number]
        print("Customer account successfully deleted.")
    else:
        print("Cannot delete customer with non-zero account balances.")

# Define a function to create a transaction (lodge or deposit)
def create_transaction():
    account_number = int(input("Enter Account Number: "))
    account_type = input("Enter Account Type (Savings/Current): ")
    amount = float(input("Enter Amount: "))
    if account_type == "Savings":
        customer_info[account_number]["Savings Account"] += amount
        print("Transaction successful. New savings balance:", customer_info[account_number]["Savings Account"])
    elif account_type == "Current":
        customer_info[account_number]["Current Account"] += amount
        print("Transaction successful. New current balance:", customer_info[account_number]["Current Account"])
    else:
        print("Invalid account type.")

# Define a function to list all customers and their account numbers
def list_customers():
    print("Customers:")
    for account_number in customer_info:
        print("Account Number:", account_number, "- Name:", customer_info[account_number]["First Name"], customer_info[account_number]["Last Name"])

# Define a function to display all customer information
def display_customers():
    for account_number in customer_info:
        print("Account Number:", account_number, "- Name:", customer_info[account_number]["First Name"], customer_info[account_number]["Last Name"])
        print("Savings Account Balance:", customer_info[account_number]["Savings Account"])
        print("Current Account Balance:", customer_info[account_number]["Current Account"])
        print("")

# Define a function to handle bank employee view
def bank_employee_view():
    while True:
        print("\nBank Employee View")
        print("1. Create Customer Account")
        print("2. Delete Customer Account")
        print("3. Create Transaction")
        print("4. List Customers and Account Numbers")
        print("5. Display All Customers")
        print("6. Exit")
        choice = int(input("Enter Choice: "))
        if choice == 1:
            create_customer()
        elif choice == 2:
            delete_customer()
        elif choice == 3:
            create_transaction()
        elif choice == 4:
            list_customers()
        elif choice == 5:
            display_customers()
        elif choice == 6:
            break
        else:
            print("Invalid Choice. Try Again.")



# Define a function to handle customer view
def customer_view():
    while True:
        print("\nCustomer View")
        first_name = input("Enter First Name: ")
        last_name = input("Enter Last Name: ")
        account_number = int(input("Enter Account Number: "))
        pin = input("Enter Pin: ")

        # Check if the customer details are correct
        if account_number in customer_info and customer_info[account_number]["First Name"] == first_name and customer_info[account_number]["Last Name"] == last_name and customer_info[account_number]["Pin"] == pin:
            while True:
                print("\nCustomer Menu")
                print("1. View Transaction History")
                print("2. Add Money to Savings Account")
                print("3. Add Money to Current Account")
                print("4. Subtract Money from Savings Account")
                print("5. Subtract Money from Current Account")
                print("6. Exit")
                choice = int(input("Enter Choice: "))
                if choice == 1:
                    account_type = input("Enter Account Type (Savings/Current): ")
                    if account_type == "Savings":
                        print("Transaction History for Savings Account:")
                        for transaction in customer_info[account_number]["Savings Transactions"]:
                            print(transaction)
                    elif account_type == "Current":
                        print("Transaction History for Current Account:")
                        for transaction in customer_info[account_number]["Current Transactions"]:
                            print(transaction)
                    else:
                        print("Invalid account type.")
                elif choice == 2:
                    amount = float(input("Enter Amount: "))
                    if amount < 0:
                        print("Invalid amount. Amount cannot be negative.")
                    else:
                        customer_info[account_number]["Savings Account"] += amount
                        customer_info[account_number]["Savings Transactions"].append(f"Added {amount} to Savings Account")
                        print("Money added to Savings Account.")
                elif choice == 3:
                    amount = float(input("Enter Amount: "))
                    if amount < 0:
                        print("Invalid amount. Amount cannot be negative.")
                    else:
                        customer_info[account_number]["Current Account"] += amount
                        customer_info[account_number]["Current Transactions"].append(f"Added {amount} to Current Account")
                        print("Money added to Current Account.")
                elif choice == 4:
                    amount = float(input("Enter Amount: "))
                    if amount < 0:
                        print("Invalid amount. Amount cannot be negative.")
                    elif amount > customer_info[account_number]["Savings Account"]:
                        print("Insufficient funds in Savings Account.")
                    else:
                        customer_info[account_number]["Savings Account"] -= amount
                        customer_info[account_number]["Savings Transactions"].append(f"Subtracted {amount} from Savings Account")
                        print("Money subtracted from Savings Account.")
                elif choice == 5:
                    amount = float(input("Enter Amount: "))
                    if amount < 0:
                        print("Invalid amount. Amount cannot be negative.")
                    elif amount > customer_info[account_number]["Current Account"]:
                        print("Insufficient funds in Current Account.")
                    else:
                        customer_info[account_number]["Current Account"] -= amount
                        customer_info[account_number]["Current Transactions"].append(f"Subtracted {amount} from Current Account")
                        print("Money subtracted from Current Account.")
                elif choice == 6:
                    break
                else:
                    print("Invalid choice.")
            break
        else:
            print("Invalid customer details.")




# Create customers.txt file if it does not exist
if not os.path.isfile('customers.txt'):
    open('customers.txt', 'w').close()

def create_customer_account(first_name, last_name, email):
    # Create account number and pin based on customer details
    initials = first_name[0] + last_name[0]
    name_length = str(len(first_name + last_name))
    first_initial_pos = str(ord(first_name[0]) - 96)
    second_initial_pos = str(ord(last_name[0]) - 96)
    pin = first_initial_pos + second_initial_pos
    
    account_number = initials + '-' + name_length + '-' + first_initial_pos + '-' + second_initial_pos
    
    # Create savings and current account files
    savings_file_name = account_number + '-savings.txt'
    current_file_name = account_number + '-current.txt'
    
    if not os.path.isfile(savings_file_name):
        with open(savings_file_name, 'w') as savings_file:
            savings_file.write('Date\tAction\tAmount\tBalance\n')
    
    if not os.path.isfile(current_file_name):
        with open(current_file_name, 'w') as current_file:
            current_file.write('Date\tAction\tAmount\tBalance\n')
    
    # Append account details to customers.txt
    with open('customers.txt', 'a') as customers_file:
        customers_file.write(account_number + '\t' + first_name + '\t' + last_name + '\t' + email + '\n')
    
    return account_number, pin









