
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report, confusion_matrix
import matplotlib.pyplot as plt

# Data loading
data = pd.read_csv("/content/IRIS_ Flower_Dataset.csv")

# Exploratory analysis of data
print(data.head())  # Displays the first lines of the DataFrame
print(data.describe())  # Displays descriptive statistics

# Verification of missing values
print(data.isnull().sum())

# Separation of characteristics (X) and target variable (y)
X = data.drop("species", axis=1)
y = data["species"]

# N# Standardization of characteristics
scaler = StandardScaler()
X = scaler.fit_transform(X)

# Division of data into training and test sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Building the SVM model
svm_model = SVC()
svm_model.fit(X_train, y_train)

# SVM model training and performance evaluation
svm_pred = svm_model.predict(X_test)
svm_accuracy = accuracy_score(y_test, svm_pred)
print("SVM Accuracy:", svm_accuracy)
print("SVM Classification Report:")
print(classification_report(y_test, svm_pred))
print("SVM Confusion Matrix:")
print(confusion_matrix(y_test, svm_pred))

# Construction of the random forest model
rf_model = RandomForestClassifier()
rf_model.fit(X_train, y_train)

# Random forest model training and performance evaluation
rf_pred = rf_model.predict(X_test)
rf_accuracy = accuracy_score(y_test, rf_pred)
print("Random Forest Accuracy:", rf_accuracy)
print("Random Forest Classification Report:")
print(classification_report(y_test, rf_pred))
print("Random Forest Confusion Matrix:")
print(confusion_matrix(y_test, rf_pred))


# Construction of the random forest model
rf_model = RandomForestClassifier()
rf_model.fit(X_train, y_train)

# Random Forest Model Training and Performance Evaluation
rf_pred = rf_model.predict(X_test)
rf_accuracy = accuracy_score(y_test, rf_pred)
print("Random Forest Accuracy:", rf_accuracy)
print("Random Forest Classification Report:")
print(classification_report(y_test, rf_pred))
print("Random Forest Confusion Matrix:")
print(confusion_matrix(y_test, rf_pred))

# Training graph for the SVM model
svm_train_acc = svm_model.score(X_train, y_train)
svm_test_acc = svm_accuracy

plt.figure(figsize=(8, 6))
plt.plot([1, 2], [svm_train_acc, svm_test_acc], marker='o')
plt.xticks([1, 2], ['Training', 'Test'])
plt.xlabel('Dataset')
plt.ylabel('Accuracy')
plt.title('SVM Model Learning Curve')
plt.ylim([0.9, 1.0])
plt.show()

# Learning plot for the random forest modelLearning plot for the random forest modelLearning plot for the random forest model
rf_train_acc = rf_model.score(X_train, y_train)
rf_test_acc = rf_accuracy

plt.figure(figsize=(8, 6))
plt.plot([1, 2], [rf_train_acc, rf_test_acc], marker='o')
plt.xticks([1, 2], ['Training', 'Test'])
plt.xlabel('Dataset')
plt.ylabel('Accuracy')
plt.title('Random Forest Model Learning Curve')
plt.ylim([0.9, 1.0])
plt.show()